function deleteLead(leadId){
  $.ajax ({
    url: '/lead/' + leadId + '/delete-json',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    data: JSON.stringify({leadId}), // to pack the data then it unpacks with parse
    type: 'POST',
    success: ((res) => {
      // Repolace follow button with unfollow :D
      console.log("Result: ", res)
      $("#"+leadId).remove();
    }),
    error: ((error) => {
      console.log("Error: ", error);
    })
  })
} 